 **# 加载 minio 服务器上的图片并显示** 

1.minio 服务器的搭建   启动后访问
  1.docker pull nginx
  2.mkdir -p /home/data
    mkdir -p /home/config
  3.docker 上minio 服务器的启动
  docker run -p 9000:9000 --name minio \
    -d --restart=always \
    -e "MINIO_ACCESS_KEY=admin" \
    -e "MINIO_SECRET_KEY=admin123456" \
    -v /home/data:/data \
    -v /home/config:/root/.minio \
    minio/minio server /data
    
  4.minio 服务器网页版的访问
    http://192.168.146.146:9000/minio/file/
    用户名：admin   密码：admin123456
    
2.minio 服务器文件的上传
3.minio 服务器文件的下载