package com.example.minio.controller;

import io.minio.MinioClient;
import io.minio.PutObjectOptions;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MinioController {
    private static String url = "http://192.168.146.146:9000/";  //minio服务的IP端口
    private static String accessKey = "admin";
    private static String secretKey = "admin123456";

    //上传文件到minio服务
    @RequestMapping ("upload")
    public String upload(@RequestParam("fileName") MultipartFile file)  {
        try {
            MinioClient minioClient = new MinioClient(url, accessKey, secretKey);
            InputStream is= file.getInputStream(); //得到文件流
            String fileName = file.getOriginalFilename(); //文件名
            String contentType = file.getContentType();  //类型
            minioClient.putObject("file",fileName,is,new PutObjectOptions(is.available(), -1)); //把文件放置Minio桶(文件夹)("file",fileName,is,contentType);
            return  "上传成功";
        }catch (Exception e){
            return "上传失败";
        }
    }
    //下载minio服务的文件
    @GetMapping("download")
    public void download(HttpServletResponse response,String fileName){
        try {
            MinioClient minioClient = new MinioClient(url, accessKey, secretKey);
            InputStream fileInputStream = minioClient.getObject("file", fileName);
            response.setHeader("Content-Disposition", "attachment;filename=" + "test.jpg");
            response.setContentType("application/force-download");
            response.setCharacterEncoding("UTF-8");
            OutputStream outputStream =response.getOutputStream();
            IOUtils.copy(fileInputStream,outputStream);
        }catch (Exception e){
           e.printStackTrace();
        }
    }
    //获取minio文件的下载地址
    @GetMapping("url")
    public  String  getUrl(){
        try {
            MinioClient minioClient = new MinioClient(url, accessKey, secretKey);
            String url = minioClient.presignedGetObject("file", "1.PNG");
            return url;
        }catch (Exception e){
            return "获取失败";
        }
    }

    //获取minio文件的下载地址
    @GetMapping("url2")
    public  String  getUrl2(){
       return "aaa";
    }

    @RequestMapping("images")
    public  String  images(Model model){
        List list = new ArrayList();
        for(int i=1;i<=8;i++){
            list.add(i+".PNG");
        }
        model.addAttribute("paths", list);
        return "/images";
    }
}
